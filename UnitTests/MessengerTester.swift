//
//  MessengerTester.swift
//  UnitTests
//
//  Created by p.koltsov on 04.12.2018.
//  Copyright © 2018 tinkoff.fts. All rights reserved.
//

import Foundation
import XCTest
import MultipeerConnectivity
@testable import MCTestingApp

class MessengerTester: CommunicatorDelegate {
    let messenger: Messenger
    
    var messagesChanged: ((MessengerTester) -> ())?
    var messages: [String] = [] {
        didSet { self.messagesChanged?(self) }
    }
    
    var onlineUsersChanged: ((MessengerTester) -> ())?
    var onlineUsers: [String: String] = [:] {
        didSet { self.onlineUsersChanged?(self) }
    }
    
    init(_ user: MCPeerID, name: String? = nil) {
        messenger = Messenger(myPeerID: user, name: name)
        messenger.delegate = self
    }
    
    func name(of user: MCPeerID) -> String? {
        return onlineUsers[Messenger.stringUserID(user)]
    }
    
    func send(_ msg: String, to: MCPeerID) {
        let result = messenger.sendMessage(text: msg, to: Messenger.stringUserID(to))
        XCTAssertTrue(result)
    }
    
    func didFoundUser(_ communicator: Communicator, userID: String, userName: String?) {
        DispatchQueue.main.async {
            self.onlineUsers[userID] = userName
        }
    }
    
    func didLostUser(_ communicator: Communicator, userID: String) {
        DispatchQueue.main.async {
            self.onlineUsers.removeValue(forKey: userID)
        }
    }
    
    func didReceiveMessage(_ communicator: Communicator, text: String, from userID: String) {
        DispatchQueue.main.async {
            self.messages.append(text)
        }
    }
    
    func didFailed(_ communicator: Communicator, with error: Error) {
    }
}
