//
//  UnitTests.swift
//  UnitTests
//
//  Created by p.koltsov on 29.11.2018.
//  Copyright © 2018 tinkoff.fts. All rights reserved.
//

import XCTest
import MultipeerConnectivity
@testable import MCTestingApp

class UnitTests: XCTestCase {

    let userID1 = MCPeerID(displayName: "iPhone_1")
    let userID2 = MCPeerID(displayName: "iPhone_2")
    
    func testSendMessage() {
        let tester1 = MessengerTester(userID1)
        let tester2 = MessengerTester(userID2)
        wait(tester1, forOnlineUser: userID2)
                
        tester1.send("123", to: self.userID2)
        wait(tester2, forMessage: "123")
    }
    
    func testReconnect() {
        let tester1 = MessengerTester(userID1)
        var tester2: MessengerTester! = MessengerTester(userID2)
        wait(tester1, forOnlineUser: userID2)
        
        tester2 = nil
        wait(tester1, forOfflineUser: userID2)
        
        tester2 = MessengerTester(userID2)
        wait(tester1, forOnlineUser: userID2)
        
        tester1.send("asd", to: self.userID2)
        wait(tester2, forMessage: "asd")
    }
    
    func testUserName() {
        let tester1 = MessengerTester(userID1, name: "User 1")
        let tester2 = MessengerTester(userID2, name: "User 2")
        wait(tester1, forOnlineUser: userID2)
        
        XCTAssertEqual("User 2 (iPhone_2)", tester1.name(of: userID2))
    }
    
    func testUserNameNil() {
        let tester1 = MessengerTester(userID1, name: "User 1")
        let tester2 = MessengerTester(userID2, name: nil)
        wait(tester1, forOnlineUser: userID2)
        
        XCTAssertEqual("iPhone_2", tester1.name(of: userID2))
    }
    
    func wait(_ tester: MessengerTester, forOnlineUser user: MCPeerID) {
        let stringID = Messenger.stringUserID(user)
        if tester.onlineUsers[stringID] != nil {
            return
        }
        let exp = expectation(description: "Wait for online")
        tester.onlineUsersChanged = { t in
            if tester.onlineUsers[stringID] != nil {
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: 3.0)
        tester.onlineUsersChanged = nil
    }
    
    func wait(_ tester: MessengerTester, forOfflineUser user: MCPeerID) {
        let stringID = Messenger.stringUserID(user)
        if tester.onlineUsers[stringID] == nil {
            return
        }
        let exp = expectation(description: "Wait for offline")
        tester.onlineUsersChanged = { t in
            if tester.onlineUsers[stringID] == nil {
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: 3.0)
        tester.onlineUsersChanged = nil
    }

    func wait(_ tester: MessengerTester, forMessage: String) {
        if let message = tester.messages.first {
            XCTAssertEqual(message, forMessage)
            tester.messages.removeFirst()
            return
        }
        
        let messageExp = self.expectation(description: "Message")
        tester.messagesChanged = { t in
            if let message = t.messages.first {
                XCTAssertEqual(message, forMessage)
                t.messagesChanged = nil
                t.messages.removeFirst()
                messageExp.fulfill()
            }
        }
        self.wait(for: [messageExp], timeout: 5.0)
    }
}
