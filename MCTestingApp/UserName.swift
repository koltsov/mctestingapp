//
//  UserName.swift
//  MCTestingApp
//
//  Created by p.koltsov on 04.12.2018.
//  Copyright © 2018 tinkoff.fts. All rights reserved.
//

import Foundation

class UserName {
    static func format(name: String?, displayName: String) -> String {
        if let name = name {
            return "\(name) (\(displayName))"
        }
        return displayName
    }
}
