//
//  Messenger.swift
//  MCTestingApp
//
//  Created by p.koltsov on 29.11.2018.
//  Copyright © 2018 tinkoff.fts. All rights reserved.
//

import Foundation
import MultipeerConnectivity

protocol CommunicatorDelegate: class {
    func didFoundUser(_ communicator: Communicator, userID: String, userName: String?)
    func didLostUser(_ communicator: Communicator, userID: String)
    func didReceiveMessage(_ communicator: Communicator, text: String, from userID: String)
    func didFailed(_ communicator: Communicator, with error: Error)
}

protocol Communicator: class {
    func sendMessage(text: String, to userID: String) -> Bool
    var delegate: CommunicatorDelegate? { get set }
}

class Messenger: NSObject, Communicator {
    
    weak var delegate: CommunicatorDelegate?
    
    private let serviceType = "tinkoff-chat"
    private let myPeerID: MCPeerID
    private let browser: MCNearbyServiceBrowser
    private let advertiser: MCNearbyServiceAdvertiser
    
    private var sessions: [NSObject: MCSession] = [:]
    private var names: [NSObject: String] = [:]
    
    init(myPeerID: MCPeerID?, name: String?) {
        self.myPeerID = myPeerID ?? Messenger.myPeerID(displayName: UIDevice.current.name)
        
        var discoveryInfo: [String: String] = [:]
        if let name = name {
            discoveryInfo["name"] = name
        }
        advertiser = MCNearbyServiceAdvertiser(peer: self.myPeerID,
                                               discoveryInfo: discoveryInfo,
                                               serviceType: serviceType)
        
        browser = MCNearbyServiceBrowser(peer: self.myPeerID, serviceType: serviceType)
        
        super.init()
        
        browser.delegate = self
        advertiser.delegate = self
        
        browser.startBrowsingForPeers()
        advertiser.startAdvertisingPeer()
    }
    
    static func myPeerID(displayName: String) -> MCPeerID {
        let defaults = UserDefaults.standard
        let key = "MyPeerID"
        
        if let data = defaults.data(forKey: key),
            let peerID = NSKeyedUnarchiver.unarchiveObject(with: data) as? MCPeerID,
            peerID.displayName == displayName {
            return peerID
        }
        
        let newPeerID = MCPeerID(displayName: displayName)
        let data = NSKeyedArchiver.archivedData(withRootObject: newPeerID)
        defaults.set(data, forKey: key)
        defaults.synchronize()
        return newPeerID
    }
    
    func sendMessage(text: String, to userID: String) -> Bool {
        do {
            let message = ["text": text]
            let jsonData = try JSONSerialization.data(withJSONObject: message)
            let peerID = self.peerID(stringID: userID)
            try sessions[peerID]?.send(jsonData, toPeers: [peerID], with: .reliable)
            return true
        } catch {
            delegate?.didFailed(self, with: error)
            return false
        }
    }
}

extension Messenger: MCNearbyServiceBrowserDelegate {
    
    static func stringUserID(_ peerID: MCPeerID) -> String {
        let data = NSKeyedArchiver.archivedData(withRootObject: peerID)
        return data.base64EncodedString()
    }
    
    func peerID(stringID: String) -> MCPeerID {
        let data = Data(base64Encoded: stringID)!
        return NSKeyedUnarchiver.unarchiveObject(with: data) as! MCPeerID
    }
    
    func browser(_ browser: MCNearbyServiceBrowser,
                 foundPeer peerID: MCPeerID,
                 withDiscoveryInfo info: [String: String]?) {

        if let name = info?["name"] as? String {
            names[peerID] = name
        }
        
        guard sessions[peerID] == nil else {
            print("browser found peer:\(peerID), session exists")
            return
        }
        
        print("browser found peer:\(peerID), inviting")
        
        let session = MCSession(peer: myPeerID)
        session.delegate = self
        sessions[peerID] = session
        browser.invitePeer(peerID, to: session, withContext: nil, timeout: 30.0)
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        print("browser lostPeer:\(peerID)")
        delegate?.didLostUser(self, userID: Messenger.stringUserID(peerID))
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        print("browser error: \(error)")
        delegate?.didFailed(self, with: error)
    }
}

extension Messenger: MCNearbyServiceAdvertiserDelegate {
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser,
                    didReceiveInvitationFromPeer peerID: MCPeerID,
                    withContext context: Data?,
                    invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        print("didReceiveInvitationFromPeer: \(peerID)")
        
        if let session = sessions[peerID] {
            print("old session will be used")
            invitationHandler(true, session)
            return
        }
        
        let session = MCSession(peer: myPeerID)
        session.delegate = self
        sessions[peerID] = session
        invitationHandler(true, session)
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser,
                    didNotStartAdvertisingPeer error: Error) {
        delegate?.didFailed(self, with: error)
    }
}

extension Messenger: MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        print("\(peerID.displayName) \(state.rawValue)")
        switch state {
        case .notConnected, .connecting:
            break
        case .connected:
            let name = names[peerID]
            let userName = UserName.format(name: name, displayName: peerID.displayName)
            delegate?.didFoundUser(self, userID: Messenger.stringUserID(peerID), userName: userName)
            break
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        do {
            let json = try JSONSerialization.jsonObject(with: data)
            let message = json as? [String: String]
            guard let text = message?["text"] else { return }
            
            DispatchQueue.main.async {
                self.delegate?.didReceiveMessage(self,
                                                 text: text,
                                                 from: Messenger.stringUserID(peerID))
            }
        } catch {
            DispatchQueue.main.async {
                self.delegate?.didFailed(self, with: error)
            }
        }
    }
    
    func session(_ session: MCSession,
                 didReceive stream: InputStream,
                 withName streamName: String,
                 fromPeer peerID: MCPeerID) {
        // unused
    }
    
    func session(_ session: MCSession,
                 didStartReceivingResourceWithName resourceName: String,
                 fromPeer peerID: MCPeerID,
                 with progress: Progress) {
        // unused
    }
    
    func session(_ session: MCSession,
                 didFinishReceivingResourceWithName resourceName: String,
                 fromPeer peerID: MCPeerID,
                 at localURL: URL?,
                 withError error: Error?) {
        // unused
    }
}
